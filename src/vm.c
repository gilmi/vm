#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "vm.h"
#include "gc.h"
#include "utils.h"


#if DEBUG
#define DEBUG_PRINT(op) \
  fprintf(stderr, "(%u) %s | sp: %d | program[ip]: %d | stack: ", vm.ip, (op), vm.sp, vm.program[vm.ip]); \
  fprint_stack(stderr, vm.sp + 1, vm.stack);
#else
#define DEBUG_PRINT(op)
#endif

#if USE_ASSERTS
#define ASSERT(name, test, excode)              \
  if (!(test)) {                                \
    fprintf(stderr, "Failed test: %s", name);   \
    exit(excode);                               \
  }
#else
#define ASSERT(name, test, excode)
#endif

#if USE_ASSERTS
#define ASSERT_STACK_SIZE(expected_items, name, vm)                 \
  if (vm.sp + 1 - expected_items < 0) {                             \
    fprintf(stderr, "Stack underflow in: (%d) %s", vm.ip, name);    \
    exit(111);                                                      \
  }
#else
#define ASSERT_STACK_SIZE(expected_items, name, vm)
#endif

int interpret(BYTE* program) {

  // Opcodes
  void* instructions[] = {
    &&halt,              //  0: Halt

    &&load_int_lit,      //  1: Load <int>
    &&load_str_lit,      //  2: Load <string>

    &&swap,              //  3: Swap top 2 items on the stack
    &&pop,               //  4: Pop the top item off the stack
    &&dup,               //  5: Pop the top item off the stack

    &&add,               //  6: Add top 2 items on the stack
    &&subtract,          //  7: Subtract top 2 items on the stack
    &&compare,           //  8: Compare top 2 items on the stack
    &&is_not_zero,       //  9: Check if the value on top of the stack is not zero

    &&print,             // 10: Print
    &&print_str,         // 11: Print the top item (which is expected to be a string)

    &&cons,              // 12: Cons <size> will create a new heap object and
                         //     will load the last <size> elements on the stack
    &&heap_index,        // 13: Index <index> will put the <index> element in
                         //     the heap object on the stack

    &&jump,              // 14: Jump to the address at the top of the stack
    &&jump_cond          // 15: Jump to the address at the top of the stack
                         //     if 2nd top item on the stack is zero
  };

  long operand1 = 0;
  long operand2 = 0;
  unsigned long uoperand = 0;
  INFO heap_info = 0;
  BYTE* heap_data = 0;
  uint16_t size_in_bytes = 0;
  uint16_t number_of_elements = 0;
  HeapObject* gen0[GEN0_SIZE];
  HeapObject* gen1[GEN1_SIZE];
  StackObject stack[STACK_SIZE] = { {0} };
  struct VM vm;
  vm.program = program;
  vm.ip = 0;
  vm.stack = stack;
  vm.sp = -1;

  vm.gen0 = gen0;
  vm.gen0p = 0;
  vm.gen1 = gen1;
  vm.gen1p = 0;

  goto *instructions[vm.program[vm.ip]];

 //  0: Halt
 halt:
  DEBUG_PRINT("halt");

  cleanup(&vm);
  return 0;

 //  1: Load <int>
 load_int_lit:
  DEBUG_PRINT("load_int_lit");

  ++vm.ip;

  uoperand = 0;
  for (unsigned i = 0, size = sizeof(long); i < size; ++i) {
    uoperand <<= 8;
    uoperand |= vm.program[vm.ip];
    ++vm.ip;
  }
  operand1 = (long)(uoperand << 1) + INTEGER_TAG;
  vm.stack[++vm.sp].immediate = operand1;

  goto *instructions[vm.program[vm.ip]];

 //  2: Load <string>
 load_str_lit:
  DEBUG_PRINT("load_str_lit");

  ++vm.ip;
  heap_info = vm.program[vm.ip++];
  heap_data = &(vm.program[++vm.ip]);

  size_in_bytes = newHeapObject(&vm.temp_ptr0, heap_info, heap_data);

  vm.ip += size_in_bytes;
  vm.stack[++vm.sp].pointer = vm.temp_ptr0;

  if (vm.gen0p >= GEN0_SIZE) {
    gen0_gc(&vm);
  }
  gen0[vm.gen0p++] = vm.temp_ptr0;

  goto *instructions[vm.program[vm.ip]];

 //  3: Swap top 2 items on the stack
 swap:
  DEBUG_PRINT("swap");
  ASSERT_STACK_SIZE(2, "swap", vm);

  vm.temp0 = vm.stack[vm.sp].immediate;
  vm.stack[vm.sp] = vm.stack[vm.sp - 1];
  vm.stack[vm.sp - 1].immediate = vm.temp0;
  goto *instructions[vm.program[++vm.ip]];

 //  4: Pop the top item off the stack
 pop:
  DEBUG_PRINT("pop");
  ASSERT_STACK_SIZE(1, "pop", vm);

  vm.stack[vm.sp].pointer = 0;
  --vm.sp;
  goto *instructions[vm.program[++vm.ip]];

 //  5: Duplicate the top item off the stack
 dup:
  DEBUG_PRINT("dup");
  ASSERT_STACK_SIZE(1, "dup", vm);

  ++vm.sp;
  vm.stack[vm.sp] = vm.stack[vm.sp - 1];
  goto *instructions[vm.program[++vm.ip]];

 //  6: Add top 2 items on the stack
 add:
  DEBUG_PRINT("add");
  ASSERT_STACK_SIZE(2, "add", vm);

  operand1 = vm.stack[vm.sp - 1].immediate;
  operand2 = vm.stack[vm.sp].immediate;
  ++vm.sp;
  vm.stack[vm.sp].immediate = (operand1 + operand2) - INTEGER_TAG;

  goto *instructions[vm.program[++vm.ip]];

 //  7: Subtract top 2 items on the stack
 subtract:
  DEBUG_PRINT("subtract");
  ASSERT_STACK_SIZE(2, "subtract", vm);

  operand1 = vm.stack[vm.sp - 1].immediate;
  operand2 = vm.stack[vm.sp].immediate;
  ++vm.sp;
  vm.stack[vm.sp].immediate = (operand1 - operand2) + INTEGER_TAG;

  goto *instructions[vm.program[++vm.ip]];

 //  8: Compare top 2 items on the stack
 compare:
  DEBUG_PRINT("compare");
  ASSERT_STACK_SIZE(2, "compare", vm);

  operand1 = vm.stack[vm.sp - 1].immediate;
  operand2 = vm.stack[vm.sp].immediate;
  ++vm.sp;
  vm.stack[vm.sp].immediate = (operand1 - operand2) + INTEGER_TAG;

  goto *instructions[vm.program[++vm.ip]];

 //  9: Check if the value on top of the stack is not zero
 is_not_zero:
  DEBUG_PRINT("is_not_zero");
  ASSERT_STACK_SIZE(2, "is_not_zero", vm);

  operand1 = vm.stack[vm.sp].immediate;
  ++vm.sp;
  if (operand1 != TRUE_VALUE) {
    vm.stack[vm.sp].immediate = TRUE_VALUE;
  } else {
    vm.stack[vm.sp].immediate = FALSE_VALUE;
  }

  goto *instructions[vm.program[++vm.ip]];

 // 10: Print
 print:
  DEBUG_PRINT("print");
  ASSERT_STACK_SIZE(1, "print", vm);

  operand1 = vm.stack[vm.sp].immediate;
  if (operand1 >= 0) {
    printf("%ld\n", (operand1 >> 1)); // to remove the least significant bit
  } else {
    printf("%ld\n", -((-(operand1 - INTEGER_TAG)) >> 1)); // to remove the least significant bit
  }
  goto *instructions[vm.program[++vm.ip]];

 // 11: Print the top item (which is expected to be a string)
 print_str:
  DEBUG_PRINT("print_str");
  ASSERT_STACK_SIZE(1, "print_str", vm);

  vm.temp_ptr0 = vm.stack[vm.sp].pointer;
  printf("%.*s\n", getHeapObjectSize(vm.temp_ptr0), ((char*)(vm.temp_ptr0->data)));

  goto *instructions[vm.program[++vm.ip]];


  // 12: Cons <size> will create a new heap object and
  //     will load the last <size> elements on the stack
 cons:
  DEBUG_PRINT("cons");

  ++vm.ip;
  heap_info = vm.program[vm.ip++];
  ++vm.ip;

  size_in_bytes = getHeapObjectSizeInBytes(heap_info);
  number_of_elements = getHeapObjectNumberOfElements(heap_info);

  ASSERT_STACK_SIZE(number_of_elements, "cons", vm);

  vm.temp_ptr0 = malloc(sizeof(INFO) + size_in_bytes); // info + data
  vm.temp_ptr0->info = heap_info;
  memcpy(vm.temp_ptr0->data, &(vm.stack[vm.sp - number_of_elements + 1]), size_in_bytes);
  vm.stack[++vm.sp].pointer = vm.temp_ptr0;

  if (vm.gen0p >= GEN0_SIZE) {
    gen0_gc(&vm);
  }
  gen0[vm.gen0p++] = vm.temp_ptr0;

  goto *instructions[vm.program[vm.ip]];

 // 13: Index <index> will put the <index> element in
 //     the heap object on the stack
 heap_index:
  DEBUG_PRINT("heap_index");
  ASSERT_STACK_SIZE(1, "heap_index", vm);

  ++vm.ip;
  heap_info = vm.program[vm.ip++];
  number_of_elements = getHeapObjectNumberOfElements(heap_info);
  ++vm.ip;
  ++vm.sp;

  vm.stack[vm.sp] = *(StackObject*)(&(vm.stack[vm.sp - 1].pointer->data[number_of_elements * sizeof(StackObject)])); // again, quite hacky.

  goto *instructions[vm.program[vm.ip]];

 // 14: Jump to the address at the top of the stack
 jump:
  DEBUG_PRINT("jump");
  ASSERT_STACK_SIZE(1, "jump", vm);

  vm.ip = as_integer(vm.stack[vm.sp--]);

  goto *instructions[vm.program[vm.ip]];

  // 15: Jump to the address at the top of the stack
  //     if 2nd top item on the stack is zero
 jump_cond:
  DEBUG_PRINT("jump_cond");
  ASSERT_STACK_SIZE(2, "jump_cond", vm);

  operand2 = as_integer(vm.stack[vm.sp--]);
  operand1 = vm.stack[vm.sp--].immediate;
  if (operand1 == TRUE_VALUE) {
    vm.ip = operand2;
  } else {
    ++vm.ip;
  }
  goto *instructions[vm.program[vm.ip]];

  // we don't expect to reach here. We expect HALT to be the last instruction
  return 1;
}
