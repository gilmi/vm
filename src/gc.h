#ifndef GC_H
#define GC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"

void gen0_gc(struct VM* vm);
void gen1_gc(struct VM* vm);
void cleanup(struct VM* vm);

bool is_gc_marked(HeapObject* obj);
void clear_gc_marked(HeapObject* obj);
void set_gc_marked(HeapObject* obj);

#endif
