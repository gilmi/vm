#ifndef VM_H
#define VM_H

#include "types.h"

// interpret a vm program.
int interpret(BYTE* program);

#endif
