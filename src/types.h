/* Types and definitions relevant to the work of the vm.
 */

#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>
#include <stdbool.h>

/* Types for objects */

// The size of a single word.
typedef uint64_t WORD;

// The information part of an object
typedef uint16_t INFO;

// A single byte.
typedef uint8_t BYTE;

// Size of objects.
typedef uint16_t SIZE;

// Size of objects in bytes.
typedef uint16_t SIZE_BYTES;

// Amount of arguments.
typedef uint8_t ARGS_AMOUNT;

// An object thar resides on the heap.
typedef struct HeapObject {
  INFO info;
  BYTE data[]; // could be either bytearray or multiple stack objects
} HeapObject;

// An object thar resides on the stack.
// Can be one of two things:
typedef union StackObject {
  // a single immediate value
  WORD immediate;
  // A pointer to a heap object.
  struct HeapObject* pointer;
} StackObject;

// The internal state of a vm.
struct VM {
  // The program we run.
  BYTE* program;
  // The instruction pointer. Points at the current instruction.
  unsigned int ip;

  // The stack.
  union StackObject* stack;
  // The stack pointer. Points at the value at the top of the stack.
  int sp;

  // The generation 0 heap. New objects are allocated here.
  struct HeapObject** gen0;
  // The generation 0 heap. Points at the next free block.
  int gen0p;

  // The generation 1 heap. Longer lived objects are moved here when gen0 is full.
  struct HeapObject** gen1;
  // The generation 1 heap. Points at the next free block.
  int gen1p;

  // Auxilary variables/registers.
  WORD temp0;
  WORD temp1;

  // Auxilary pointer.
  struct HeapObject* temp_ptr0;
};

/* Query stack objects */

bool is_integer(StackObject obj);
bool is_pointer(StackObject obj);

// Cast stack object as integer.
long as_integer(StackObject obj);

// Allocate and add an object to the heap. May trigger garbage collection.
SIZE newHeapObject(HeapObject** obj, INFO info, const BYTE* data);

/* Query objects */

SIZE_BYTES getHeapObjectSize(HeapObject* ptr);
SIZE_BYTES getHeapObjectSizeInBytes(INFO info);
SIZE getHeapObjectNumberOfElements(INFO info);

bool is_heap_array(HeapObject* obj);
bool is_bytearray(HeapObject* obj);
bool is_closure(HeapObject* obj);
bool checkInfoTag(INFO info, INFO tag);

uint16_t removeTags(INFO info);
uint16_t removeGcBit(INFO info);

ARGS_AMOUNT getClosureArgSize(HeapObject* obj);
ARGS_AMOUNT getClosureAppliedNum(HeapObject* obj);

/* Debugging */

#define DEBUG 0
#define USE_ASSERTS 1

/* Global settings */

#define PROGRAM_SIZE 1024*1024

#define GEN0_SIZE 4 // We'll keep it small for testing purposes
#define GEN1_SIZE 32
#define STACK_SIZE 1024

/* Object tags */

// For heap objects
#define GC_MARKED_TAG 0b1
#define HEAPARRAY_TAG 0b000 // array of heap objects
#define BYTEARRAY_TAG 0b001 // array of bytes
#define CLOSURE_TAG 0b010   // a closure

// For stack objects
#define INTEGER_TAG 0b1
#define POINTER_TAG 0b0

// For stack objects
#define TRUE_VALUE  (WORD)0b1
#define FALSE_VALUE (WORD)0b11

#endif
