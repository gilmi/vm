/* Entrypoint to the vm.
 */

#include <stdio.h>
#include <stdint.h>
#include "vm.h"

// Read a vm bytecode and run it.
int main(int argc, char* argv[]) {
  if (argc != 2) {
    puts("Expecting the first argument to be the input file.\n");
    return 2;
  }
  char* p = argv[1];

  FILE *fp = fopen(p, "r");
  uint8_t program[PROGRAM_SIZE];
  fread(program, sizeof(program), 1, fp);
  fclose(fp);

  return interpret(program);
}
