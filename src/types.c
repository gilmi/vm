#include "types.h"
#include <stdlib.h>
#include <string.h>

// Stack Objects //

bool is_integer(StackObject obj) {
  return (obj.immediate & INTEGER_TAG) == INTEGER_TAG;
}
bool is_pointer(StackObject obj) {
  return (obj.immediate & POINTER_TAG) == POINTER_TAG;
}

long as_integer(StackObject obj) {
  return (obj.immediate >> 1);
}

// Heap Objects //

SIZE_BYTES newHeapObject(HeapObject** obj, INFO info, const BYTE* data) {
  SIZE_BYTES size_in_bytes = getHeapObjectSizeInBytes(info);
  *obj = malloc(sizeof(INFO) + size_in_bytes); // info + data
  (*obj)->info = info;
  memcpy((*obj)->data, data, size_in_bytes);
  return size_in_bytes;
}

SIZE_BYTES getHeapObjectSize(HeapObject* ptr) {
  return getHeapObjectSizeInBytes(ptr->info);
}

SIZE_BYTES getHeapObjectSizeInBytes(INFO info) {
  if (checkInfoTag(info, BYTEARRAY_TAG)) {
    return getHeapObjectNumberOfElements(info);
  } else if (checkInfoTag(info, HEAPARRAY_TAG)) {
    return getHeapObjectNumberOfElements(info) * sizeof(StackObject);
  } else { // closure
    return getHeapObjectNumberOfElements(info) * sizeof(StackObject);
  }
}

// how many elements are in the array
SIZE getHeapObjectNumberOfElements(INFO info) {
  if (checkInfoTag(info, BYTEARRAY_TAG)) {
    return removeTags(info);
  } else if (checkInfoTag(info, HEAPARRAY_TAG)) {
    return removeTags(info);
  } else { // closure, take the current number of args
    return removeTags(info) & 0b111111;
  }
  return removeTags(info);
}

// Heap Object Tags

bool checkInfoTag(INFO info, INFO tag) {
  return ((removeGcBit(info) & tag) == tag);
}
bool is_bytearray(HeapObject* obj) {
  return checkInfoTag(obj->info, BYTEARRAY_TAG);
}
bool is_heap_array(HeapObject* obj) {
  return checkInfoTag(obj->info, HEAPARRAY_TAG);
}
bool is_closure(HeapObject* obj) {
  return checkInfoTag(obj->info, CLOSURE_TAG);
}
uint16_t removeTags(INFO info) {
  return (info >> 4);
}
uint16_t removeGcBit(INFO info) {
  return (info >> 1);
}

// Closures

uint8_t getClosureArgSize(HeapObject* obj) {
  return removeTags(obj->info >> 6);
}

uint8_t getClosureAppliedNum(HeapObject* obj) {
  return (removeTags(obj->info) & 0b111111);
}
