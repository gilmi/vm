{-# LANGUAGE OverloadedStrings #-}

module TestUtil
  ( CompareResult
  , mkCompareResult
  , module Test.Hspec
  )
  where

import Test.Hspec
import System.Process
import System.Directory
import Control.Monad (when)
import Control.Exception (finally)
import GHC.Conc (newTVarIO, atomically, readTVar, writeTVar)
import GlideVM

type CompareResult = Program -> [String] -> IO ()

mkCompareResult :: Bool -> IO (Program -> [String] -> IO ())
mkCompareResult deleteWhenDone = do
  tmpdir <- getTemporaryDirectory
  let testdir = tmpdir <> "/" <> "myvmtestsdir"
  createDirectoryIfMissing False testdir
  var <- newTVarIO @Int 0
  pure $ \code shouldbe -> do
    i <- atomically $ do
      i <- readTVar var
      writeTVar var (i + 1)
      pure i
    let file = testdir <> "/" <> "test" <> show i <> ".bin"
    writeBytesToFile file . compile $ code
    finally
      ( do
        result <- readProcess "_build/vm" [file] ""
        result `shouldBe` unlines shouldbe
      )
      (when deleteWhenDone $ removeFile file)
