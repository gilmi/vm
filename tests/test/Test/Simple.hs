{-# LANGUAGE OverloadedStrings #-}

module Test.Simple (spec) where

import TestUtil
import GlideVM

spec :: CompareResult -> Spec
spec compareResult = do
  describe "Simple" $ do
    describe "load integers and print" $ do
      it "prints 17" $ do
        compareResult
          [ Lit 17, Print, Halt ]
          [ "17" ]

      it "prints 1000" $ do
        compareResult
          [ Lit 1000, Print, Halt ]
          [ "1000" ]

      it "prints -1000" $ do
        compareResult
          [ Lit (-1000), Print, Halt ]
          [ "-1000" ]

    it "swap 1 2" $ do
      compareResult
        [ Lit 1, Lit 2, Swap, Print, Halt ]
        [ "1" ]

    it "dup 1 sub" $ do
      compareResult
        [ Lit 1, Dup, Subtract, Print, Halt ]
        [ "0" ]

    arith compareResult

    comparison compareResult

arith :: CompareResult -> Spec
arith compareResult = do
  describe "arith" $ do
    describe "add" $ do
      it "add (1 + 2)" $ do
        compareResult
          [ Lit 1, Lit 2, Add, Print, Halt ]
          [ "3" ]

      it "add negatives ((-112) + (-23))" $ do
        compareResult
          [ Lit (-112), Lit (-23), Add, Print, Halt ]
          [ "-135" ]

    describe "subtract" $ do
      it "subtract (17 - 9)" $ do
        compareResult
          [ Lit 17, Lit 9, Subtract, Print, Halt ]
          [ "8" ]

      it "subtract to negative (9 - 17)" $ do
        compareResult
          [ Lit 9, Lit 17, Subtract, Print, Halt ]
          [ "-8" ]

      it "subtract pos neg (101 - (-52))" $ do
        compareResult
          [ Lit 101, Lit (-52), Subtract, Print, Halt ]
          [ "153" ]

      it "subtract neg neg (-101 - (-52))" $ do
        compareResult
          [ Lit (-101), Lit (-52), Subtract, Print, Halt ]
          [ "-49" ]

    describe "mix" $ do
      it "add subtract (1 + (1 - 2))" $ do
        compareResult
          [ Lit 1, Lit 1, Lit 2, Subtract, Add, Print, Halt ]
          [ "0" ]

      it "add many (1 + (2 + (3 + (4 + 5))))" $ do
        compareResult
          [ Lit 1, Lit 2, Lit 3, Lit 4, Lit 5, Add, Add, Add, Add, Print, Halt ]
          [ "15" ]

comparison :: CompareResult -> Spec
comparison compareResult = do
  describe "comparison" $ do
    it "compare 1 2" $ do
      compareResult
        [ Lit 1, Lit 2, Compare, Print, Halt ]
        [ "-1" ]

    it "compare 137 137" $ do
      compareResult
        [ Lit 137, Lit 137, Compare, Print, Halt ]
        [ "0" ]

    it "compare 137 135" $ do
      compareResult
        [ Lit 137, Lit 135, Compare, Print, Halt ]
        [ "2" ]
