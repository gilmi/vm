{-# LANGUAGE OverloadedStrings #-}

module Test.GC (spec) where

import qualified Data.ByteString.Char8 as BSC
import TestUtil
import GlideVM

spec :: CompareResult -> Spec
spec compareResult = do
  describe "Garbage collection" $ do
    it "print a few lines" $ do
      let
        code =
          [ LitStr "hello",  PrintStr
          , LitStr "world",  PrintStr, Pop
          , LitStr "how",    PrintStr, Pop
          , LitStr "are",    PrintStr
          , LitStr "you",    PrintStr, Pop
          , LitStr "today?", PrintStr, Pop
          , Halt
          ]
        expectedResult =
          [ "hello"
          , "world"
          , "how"
          , "are"
          , "you"
          , "today?"
          ]
      code `compareResult` expectedResult

    it "print a few more lines" $ do
      let
        code =
          [ LitStr "hello",       PrintStr
          , LitStr "world",       PrintStr, Pop
          , LitStr "how",         PrintStr, Pop
          , LitStr "are",         PrintStr
          , LitStr "you",         PrintStr, Pop
          , LitStr "today?",      PrintStr, Pop
          , LitStr "let",         PrintStr
          , LitStr "me",          PrintStr, Pop
          , LitStr "tell",        PrintStr, Pop
          , LitStr "you",         PrintStr
          , LitStr "something",   PrintStr, Pop
          , LitStr "interesting", PrintStr, Pop
          , LitStr "about",       PrintStr, Pop
          , LitStr "fish?",       PrintStr, Pop
          , LitStr "i dunno",     PrintStr
          , LitStr "have ",       PrintStr, Pop
          , LitStr " fun",        PrintStr, Pop
          , Halt
          ]
        expectedResult =
          [ "hello"
          , "world"
          , "how"
          , "are"
          , "you"
          , "today?"
          , "let"
          , "me"
          , "tell"
          , "you"
          , "something"
          , "interesting"
          , "about"
          , "fish?"
          , "i dunno"
          , "have "
          , " fun"
          ]
      code `compareResult` expectedResult

  -- We want to test the gen1 gc
  -- It currently has the size of 32
  -- We want to load many literals to trigger the gen0 gc
  -- But we want to get rid of them at some point
    it "gen0 + gen1 gc" $ do
      let
        strings = (<>)
          [ LitStr (BSC.pack [c]) | c <- ['a'..'e'] ]
          [ Swap, Pop, Swap, Pop, Swap, Pop ]
        code = take 120 (cycle strings) <> [PrintStr, Halt]

      code `compareResult` ["b"]
