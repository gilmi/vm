{-# LANGUAGE OverloadedStrings #-}

module Test.Lists (spec) where

import TestUtil
import GlideVM

spec :: CompareResult -> Spec
spec compareResult = do
  describe "Linked lists" $ do
    it "simple list" $ do
      let
        code = [Lit 1, Lit 2, Nil, Cons 2, Cons 2, HeapIndex 0, Print, Halt] -- (1 : 2 : Nil)
      code `compareResult` ["1"]


    it "simple list 2" $ do
      let
        code =
          [ Lit 1, Lit 2, Nil, Cons 2, Cons 2  -- (1 : 2 : Nil)
          , HeapIndex 0, Print, Pop
          , HeapIndex 1, HeapIndex 0, Print, Pop, Pop
          , Halt
          ]

      code `compareResult` ["1", "2"]
