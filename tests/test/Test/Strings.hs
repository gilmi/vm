{-# LANGUAGE OverloadedStrings #-}

module Test.Strings (spec) where

import TestUtil
import GlideVM

spec :: CompareResult -> Spec
spec compareResult = do
  describe "Strings" $ do
    it "print \"hello\"" $ do
      compareResult
        [ LitStr "hello", PrintStr, Halt ]
        [ "hello" ]

    it "print \"hello\" \"world\"" $ do
      let
        code =
          [ LitStr "hello", PrintStr, Pop
          , LitStr "world", PrintStr, Pop
          , Halt
          ]
        expectedResult =
          [ "hello"
          , "world"
          ]
      code `compareResult` expectedResult
