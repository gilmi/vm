{-# LANGUAGE OverloadedStrings #-}

module Test.Jumps (spec) where

import TestUtil
import GlideVM

spec :: CompareResult -> Spec
spec compareResult = do
  describe "Jumps" $ do
    it "skip an op" $ do
      compareResult
        [ Lit 17, Jump "main", Lit 0, Labelled "main" Print, Halt ]
        [ "17" ]

    it "jump forward -> back -> end" $ do
      compareResult
        [ Jump "main"
        , Labelled "cont" $ Lit 2, Print, Jump "end"
        , Labelled "main" $ Lit 1, Print, Jump "cont"
        , Labelled "end" Halt
        ]
        [ "1", "2" ]

    conditionalJumps compareResult

conditionalJumps :: CompareResult -> Spec
conditionalJumps compareResult = do
  describe "jump_cond" $ do
    it "jump not zero" $ do
      compareResult
        [ Lit 0, Lit 17, Compare, IsNotZero, Swap, Pop, JumpCond "main"
        , Lit 0, Labelled "main" Print, Halt
        ]
        [ "17" ]

    it "loop 1..10" $ do
      compareResult
        [ Lit 10, Lit 0
        , Labelled "begin" $ Lit 1, Add, Print
        , Compare, IsNotZero, Swap, Pop, JumpCond "begin"
        , Halt
        ]
        (map @Int show [1..10])
