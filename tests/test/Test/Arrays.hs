{-# LANGUAGE OverloadedStrings #-}

module Test.Arrays (spec) where

import TestUtil
import GlideVM

spec :: CompareResult -> Spec
spec compareResult = do
  describe "Arrays" $ do
    it "An array" $ do
      let
        code = concat
          [ map Lit [0..9]
          , [ Cons 10
            , HeapIndex 7, Print
            , Halt
            ]
          ]
      code `compareResult` ["7"]

    it "A 2d array" $ do
      let
        size = 10
        index = 2
        code = concat
          [ concatMap (\n -> [Lit n, Lit n, Lit n, Cons 3]) [0..(size - 1)]
          , [ Cons (fromIntegral size)
            , HeapIndex index, HeapIndex 1, Print
            , Halt
            ]
          ]
      code `compareResult` [show index]

    it "A 2d array throwaway" $ do
      let
        size = 10
        index = 2
        code = concat
          [ concatMap (\n -> [Lit n, Lit n, Lit n, Cons 3]) [0..(size - 1)]
          , [ Cons (fromIntegral size)
            , Pop
            ]
          , concatMap (\n -> [Lit n, Lit n, Lit n, Cons 3]) [0..(size - 1)]
          , [ Cons (fromIntegral size)
            , HeapIndex index, HeapIndex 1, Print
            , Halt
            ]
          ]
      code `compareResult` [show index]

    it "A string array throwaway" $ do
      let
        size = 5 :: Int
        index = 0
        allocArrayCode = concat
          [ concatMap
            (\_ ->
               [LitStr "hello", LitStr "world", LitStr "aaaaaaaaa", LitStr "bye", LitStr "now", Cons 5]
            )
            [0..(size - 1)]
          , [ Cons (fromIntegral size)
            ]
          ]
        code = concat
          [ allocArrayCode
          , [ HeapIndex index, HeapIndex 1
            , Swap, Pop, Swap, Pop
            ]
          , allocArrayCode
          , [ Swap
            , PrintStr
            , Halt
            ]
          ]
      code `compareResult` ["world"]
