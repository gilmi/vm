#!/usr/bin/env cabal
{- cabal:
build-depends:
  base,
  bytestring,
  cereal,
  containers,
  directory,
  hspec,
  process
-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

import TestUtil
import qualified Test.Simple as Simple
import qualified Test.Strings as Strings
import qualified Test.GC as GC
import qualified Test.Lists as Lists
import qualified Test.Arrays as Arrays
import qualified Test.Jumps as Jumps

main :: IO ()
main = do
  compareResult <- mkCompareResult True
  hspec $ parallel $ do
    Simple.spec compareResult
    heap compareResult

heap :: CompareResult -> Spec
heap compareResult = do
  describe "Heap objects" $ do
    Strings.spec compareResult
    GC.spec compareResult
    Lists.spec compareResult
    Arrays.spec compareResult
    Jumps.spec compareResult
