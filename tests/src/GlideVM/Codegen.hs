{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module GlideVM.Codegen
  ( compile
  , writeBytesToFile
  )
where

import Data.Serialize
import Data.ByteString qualified as BS
import GlideVM.Sugared qualified as Sugared
import GlideVM.Desugared qualified as Desugared
import GlideVM.Serialize

-------------
-- Compile --
-------------

compile :: Sugared.Program -> Put
compile =
  ( mapM_ compileStmt
  . concatMap desugarStmt
  . either error id
  . Sugared.desugarAddresses
  )

writeBytesToFile :: FilePath -> Put -> IO ()
writeBytesToFile file =
  BS.writeFile file . runPut

---

desugarStmt :: Sugared.Stmt' Sugared.Address -> [Desugared.Stmt]
desugarStmt = \case
  Sugared.Halt -> [Desugared.Halt]
  Sugared.Lit n -> [Desugared.Load n]
  Sugared.LitStr str -> [Desugared.LitStr str]
  Sugared.Swap -> [Desugared.Swap]
  Sugared.Pop -> [Desugared.Pop]
  Sugared.Dup -> [Desugared.Dup]
  Sugared.Add ->
    [Desugared.Add, Desugared.Swap, Desugared.Pop, Desugared.Swap, Desugared.Pop]
  Sugared.Subtract ->
    [Desugared.Subtract, Desugared.Swap, Desugared.Pop, Desugared.Swap, Desugared.Pop]
  Sugared.Compare -> [Desugared.Compare]
  Sugared.IsNotZero -> [Desugared.IsNotZero]
  Sugared.Print -> [Desugared.Print]
  Sugared.PrintStr -> [Desugared.PrintStr]
  Sugared.Cons size ->
    ( Desugared.Cons size
    : take (fromIntegral size * 2) (cycle [Desugared.Swap, Desugared.Pop])
    )
  Sugared.HeapIndex i -> [Desugared.HeapIndex i]
  Sugared.Jump address ->
    [ Desugared.Load (fromIntegral address)
    , Desugared.Jump
    ]
  Sugared.JumpCond address ->
    [ Desugared.Load (fromIntegral address)
    , Desugared.JumpCond
    ]
  Sugared.Nil -> [Desugared.Load 0]
  Sugared.Labelled _ stmt -> desugarStmt stmt

compileStmt :: Desugared.Stmt -> Put
compileStmt = \case
  Desugared.Halt -> halt_
  Desugared.Load n -> load_ n
  Desugared.LitStr str -> str_ str
  Desugared.Swap -> swap_
  Desugared.Pop -> pop_
  Desugared.Dup -> dup_
  Desugared.Add -> add_
  Desugared.Subtract -> subtract_
  Desugared.Compare -> compare_
  Desugared.IsNotZero -> is_not_zero_
  Desugared.Print -> print_
  Desugared.PrintStr -> printStr_
  Desugared.Cons size -> cons_ size
  Desugared.HeapIndex i -> index_ i
  Desugared.Jump -> jump_
  Desugared.JumpCond -> jumpCond_
