{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}

module GlideVM.Sugared where

import Data.Word
import Data.Int
import qualified Data.Map as M
import qualified Data.ByteString.Char8 as BSC

type Program = [Stmt]
type Stmt = Stmt' Label

data Stmt' label
  = Halt
  | Lit Int64
  | LitStr BSC.ByteString
  | Swap
  | Pop
  | Dup
  | Add
  | Subtract
  | Compare
  | IsNotZero
  | Print
  | PrintStr
  | Cons Word16
  | HeapIndex Word16
  | Jump label
  | JumpCond label
  | Nil
  | Labelled label (Stmt' label)
  deriving (Show, Read, Functor, Foldable, Traversable)

------------
-- Labels --
------------

stmtWidth :: Stmt -> Word32
stmtWidth = (1+) . \case
  Halt -> 0
  Lit{} -> 8
  LitStr bs -> 2 + fromIntegral (BSC.length bs)
  Swap -> 0
  Pop -> 0
  Dup -> 0
  Add -> 0 + sum (map stmtWidth [Swap, Pop, Swap, Pop])
  Subtract -> 0 + sum (map stmtWidth [Swap, Pop, Swap, Pop])
  Compare -> 0
  IsNotZero -> 0
  Print -> 0
  PrintStr -> 0
  Cons size -> 2 + sum (map stmtWidth $ take (fromIntegral size * 2) (cycle [Swap, Pop]))
  HeapIndex{} -> 2
  Jump{} -> stmtWidth (Lit 0) + 0
  JumpCond{} -> stmtWidth (Lit 0) + 0
  Nil -> stmtWidth (Lit 0)
  Labelled _ stmt -> stmtWidth stmt

type LabelMap = M.Map Label Word32

type Label = String
type Address = Word32

desugarAddresses :: [Stmt] -> Either String [Stmt' Address]
desugarAddresses stmts = do
  mapping <- calculateAddresses mempty 0 stmts
  assignAddresses mapping stmts

calculateAddresses :: LabelMap -> Word32 -> [Stmt] -> Either String LabelMap
calculateAddresses labelMap address stmts =
  case stmts of
    Labelled name inner : rest ->
      case M.lookup name labelMap of
        Nothing ->
          calculateAddresses (M.insert name address labelMap) address (inner : rest)
        Just address' ->
          Left $ unlines
            [ "Multiple label definitions for label '" <> name <> "'."
            , "At address: " <> show address'
            , "And also at: " <> show address
            , "With statement: " <> show inner
            ]
    stmt : rest ->
      calculateAddresses labelMap (stmtWidth stmt + address) rest
    [] ->
      pure labelMap

assignAddresses :: LabelMap -> [Stmt] -> Either String [Stmt' Address]
assignAddresses labelMap =
  traverse $ traverse $ \lbl ->
    maybe
      ( Left $ unlines
        [ "Label not defined: '" <> lbl <> "'."
        , "In map: " <> show labelMap
        ]
      )
      pure
      (M.lookup lbl labelMap)
