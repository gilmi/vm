{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module GlideVM.Serialize where

import Data.Word
import Data.Bits
import Data.Int
import Data.Serialize
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC

---------------
-- Serialize --
---------------

halt_  :: Put
halt_  = putWord8 0

load_ :: Int64 -> Put
load_ n = do
  putWord8 1
  putInt64be n

str_ :: BS.ByteString -> Put
str_ str = do
  putWord8 2 -- opcode
  putWord16le (markByteArray (addInfoTag (fromIntegral (BSC.length str))))
  mapM_ (putInt8 . fromIntegral) (BS.unpack str)

swap_ :: Put
swap_ = putWord8 3

pop_ :: Put
pop_ = putWord8 4

dup_ :: Put
dup_ = putWord8 5

add_ :: Put
add_ = putWord8 6

subtract_ :: Put
subtract_ = putWord8 7

compare_ :: Put
compare_ = putWord8 8

is_not_zero_ :: Put
is_not_zero_ = putWord8 9

print_ :: Put
print_ = putWord8 10

printStr_ :: Put
printStr_ = putWord8 11

cons_ :: Word16 -> Put
cons_ size = do
  putWord8 12 -- opcode
  putWord16le (fromIntegral (addInfoTag size))

index_ :: Word16 -> Put
index_ i = do
  putWord8 13 -- opcode
  putWord16le (addInfoTag i)

jump_ :: Put
jump_ = putWord8 14

jumpCond_ :: Put
jumpCond_ = putWord8 15

---

addInfoTag :: Word16 -> Word16
addInfoTag n = shiftL n 4

markByteArray :: Word16 -> Word16
markByteArray num = setBit num 1
