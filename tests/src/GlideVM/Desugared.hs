{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module GlideVM.Desugared where

import Data.Word
import Data.Int
import qualified Data.ByteString.Char8 as BSC

type Program = [Stmt]

data Stmt
  = Halt
  | Load Int64
  | LitStr BSC.ByteString
  | Swap
  | Pop
  | Dup
  | Add
  | Subtract
  | Compare
  | IsNotZero
  | Print
  | PrintStr
  | Cons Word16
  | HeapIndex Word16
  | Jump
  | JumpCond
  deriving (Show, Read)

type Address = Word32
